# Greco Tape Emulator Reverse Engineering

I got some fairly nondescript Z80 boards a number of years ago, and I've
finally come around to reverse engineering them a bit. They are likely out of
a Greco Systems Minifile or DU-58 tape emulator, but I'm not entirely sure
which as there is so little public information on these. If this is true, then
these boards would have been designed to emulate a DEC TU-58 tape interface,
backing the virtual tape with (files on a?) floppy disk\*.


No matter if this is true, I'm less interested in reverse engineering the
original behaviour of the entire system, and more interested with reversing it
just to the point where I can put custom software on the thing.

## Boards

There are two boards that look as if they would have been interconnected by a
backplane. One board contains the Z80 with its peripherals. The other is a more
"dumb" I/O board possibly for a punched tape interface.

### Board 1: Z80 board

This houses:

1. Z80 CPU
1. Two Z80 PIO chips
1. Z80 DMA controller
1. WD2797 floppy controller
1. Z80 DART
1. Z80 CTC
1. 24 kB of SRAM
1. 32 kB of PROM
1. 512 bytes of EEPROM
1. Crystal oscillator with dual outputs 4, 8 MHz. But only 4 is used.
1. Two PLAs (PLS153N) whose [program I've reversed](pla/)
1. Tristate buffer/transceiver for 8-bit backplane data
1. Buffers for full 16-bit backplane address
1. Another buffer I'm still reversing, but is likely full of control signals (MREQ etc.) for backplane
1. Level shifters for what looks like dual RS232 (dual 2x8 boxed headers) into the DART.
1. Level shifters/buffers for a 34-pin boxed floppy header (confirmation of standard pinout pending)
1. Another mystery boxed 2x8 header, might just be power input.

### Board 2: Some I/O board

This contains a single PLA, a bunch of jellybean logic ICs, and in one corner
it looks to be farming transistors and capacitors - some sort of voltage supply
or pump? It has a 2x25 boxed male connector which I presume to be related to
some sort of punched tape interface.

---


\* Sidenote: the irony here is that today in 2022 with floppies and drives
becoming harder to find, and existing floppy drives becoming even less reliable
than they already weren't in their heyday, it would be best for me to back the
floppy interface on this thing with a floppy emulator that stores virtual
floppies on something like a USB stick or SD card.

