# PLAs

There are two PLS153N PLAs on the main logic board. U1A and U4A.
There is a third on the mystery I/O board which I am still to even come close
to looking at reversing.

## pla-scan.py

This is a quick inflexible script I wrote to get my GreatFET to exhaustively
scan each PLA. In general, I assumed that the PLAs don't store state internally
(though this is probably achievable looking at the PLA architecture) since they
are positioned in fairly mundane address decoding duty. For extra surity
though, I've added a single pass of random lookup after the initial scan. If
you're super paranoid, then just increase the number of these random passes to
try and incite statefulness inside the PLA giving readout verification errors.

This script spits out JSON mapping all possible input combinations to their
output. What's missing is signal labels. For now these labels are
implicit/known between this script and the sim.


## pla-sim.py

Since these PLAs are fairly small, I didn't spend too much time looking for
software/libs that could automatically reverse each PLA output's expression for
me. Instead, manually derived the expressions. After some iteration, I settled
on some fairly minimal expressions for each output which leaves each PLA's sim
exactly matching the actual PLA's readout.

These scripts don't really do much aside from documenting each PLA's program.
See `test.sh`:

    $ ./pla-sim.py data
    Reading PLA JSON dump from stdin for data mode...
    Records read: 2048
    Errors: 0
    $ ./pla-sim.py address
    Reading PLA JSON dump from stdin for address mode...
    Records read: 512
    Errors: 0

I dubbed U1A the "data" PLA and U4A the "address" PLA. The former is more
concerned with IORQ/peripheral chip selects, while the latter is interested in
memory device chip selects. Both PLAs hold interest in the backplane data bus
direction.


## U1A

This has a marking of 50005 stamped on, presuambly indicating its program.

This takes care of IORQ decode. Its inputs are:

1. `A0`-`A7`: Lower 8 bits of address bus
1. `nM1`: Active-low indication of CPU in M1 state
1. `nIORQ`: Active-low indication of IO request
1. `nRD`: Active-low indication of a read operation

Its outputs are:

1. `nCE_CTC`: Active-low chip enable for CTC U6B
1. `nCE_DART`: Active-low chip enable for DART U6C
1. `nCE_PIO2`: Active-low chip enable for PIO U2C
1. `nCE_FLOP`: Active-low chip enable for Floppy controller U3C
1. `nCE_DMA`: Active-low chip enable for DMA controller U5C
1. `nCE_PIO1`: Active-low chip enable for PIO U1C
1. Backplane data bus direction. Feeds into Pin 19 of U4A for gating on `nMREQ`


The chip selects going low is predicated on `nM1 and not nIORQ`, and are then
decided on the lower 8 bits of the address bus. Thus, the IO port map is as
follows:

| port | Register    |
| ---- | ----------- |
| 0xe8 | PIO1 U1C Port A data    |
| 0xe9 | PIO1 U1C Port A control |
| 0xea | PIO1 U1C Port B data    |
| 0xeb | PIO1 U1C Port B control |
| 0xec | CTC Channel 0           |
| 0xed | CTC Channel 1           |
| 0xee | CTC Channel 2           |
| 0xef | CTC Channel 3           |
| 0xf0 | DART Port A Data        |
| 0xf1 | DART Port A Control     |
| 0xf2 | DART Port B Data        |
| 0xf3 | DART Port B Control     |
| 0xf4 | PIO2 U2C Port A data    |
| 0xf5 | PIO2 U2C Port A control |
| 0xf6 | PIO2 U2C Port B data    |
| 0xf7 | PIO2 U2C Port B control |
| 0xf8 | Floppy command/status   |
| 0xf9 | Floppy track            |
| 0xfa | Floppy sector           |
| 0xfb | Floppy data             |
| 0xfc | DMA ??                  |
| 0xfd | DMA ??                  |
| 0xfe | DMA ??                  |
| 0xff | DMA ??                  |


## U4A

This has a marking of 50031 stamped on, presuambly indicating its program.

This takes care of MREQ decode, as well as gating the data bus direction on the
backplane.

Its inputs are:

1. `A11`-`A15`: Upper 5 bits of address bus
1. `nMREQ`: Active-low indication of a memory request
1. `nWR`: Active-low indication of a write operation
1. `nRFSH`: Active-low indication of a memory refresh
1. Backblane data bus direction fed from Pin 16 of U1A.


Its outputs are:

1. `nCE_ROM`: Active-low chip enable for PROM U5B
1. `nCE_RAM0`: Active-low chip enable for RAM U4B
1. `nCE_RAM1`: Active-low chip enable for RAM U3B
1. `nCE_RAM2`: Active-low chip enable for RAM U2B
1. `nCE_EE`: Active-low chip enable for EEPROM U1B
1. `A14_nWE`: Combined signal, A14 if addressing ROM, nWR if addressing RAM
1. `BP_D_DIR`: Backplane data direction signal. High for CPU writes to BP, low for CPU reads from BP.
1. `M_A11`: Combined signal. A11 if addressing ROM or RAM, otherwise nWR if addressing EEPROM
