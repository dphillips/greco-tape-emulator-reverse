#!/usr/bin/env python

import json
import sys
import argparse


def bools_to_bitstring(bools):
    """Make a stringy bitstring from an list of bools

    >>> bools_to_bitstring([True, True, False, False, True, False, False])
    '1100100'
    >>> bools_to_bitstring([])
    ''
    """
    return ''.join('1' if bit else '0' for bit in bools)


def bitstring_to_bools(bitstring):
    """Make a list of bools from a stringy bitstring

    >>> bitstring_to_bools('1100100')
    [True, True, False, False, True, False, False]
    >>> bitstring_to_bools('')
    []
    """
    return list(map(lambda x: x == '1', bitstring))


def do_address_pla(bits):
    (U4AP19, nRFSH, nWR, nMREQ, a15, a14, a13, a12, a11) = bits
    # most of these are gated on nRFSH and not nMREQ, i.e. "memory is requested
    # and not a refresh cycle". Not selecting SRAM during a refresh cycle is a
    # common trick to reduce power usage since SRAM doesn't need refreshing.
    is_real_mreq = nRFSH and not nMREQ
    return [

        # Simple CS for memory chips:
        # 0x0000-0x7fff selects PROM if not writing:
        not (is_real_mreq and nWR and not a15),

        # 0x8000-0x9fff selects RAM0
        not (is_real_mreq and a15 and not a14 and not a13),

        # 0xa000-bfff selects RAM1
        not (is_real_mreq and a15 and not a14 and a13),

        # 0xc000-dfff selects RAM2
        not (is_real_mreq and a15 and a14 and not a13),

        # 0xe000-f7ff would appear to be dead

        # 0xf800-ffff selects EEPROM
        not (is_real_mreq and
             a15 and a14 and a13 and a12 and a11),

        # P15, unused
        True,

        # A14_nWE is dual purpose: either A14 or nWE depending on addr bus
        # if not a15, then a14 (i.e. ROM means a14)
        # if (a15:a14:a13) from 100 to 110, then nWR (i.e. RAM means nWR)
        not (is_real_mreq and (
            (not a15 and not a14) or
            (a15 and not (a13 and a14) and not nWR)
            )),

        # BP_D_DIR:
        # 1. CPU->BP if no memory req in progress or if U4AP19
        # 2. else BP->CPU
        not nMREQ or U4AP19,

        # M_A11: memory bank a11 or EEPROM nWR
        # 1. pass thrugh a11 from CPU if memory requested and we're on a ROM or
        #    RAM block, or
        # 2. pass throgh nWR from CPU if memory requested and we're on the
        #     EEPROM
        not (is_real_mreq and (
            not a11 and (
                (not a15) or  # ROM
                (a15 and not a14) or  # 11x => RAM0 and RAM1
                (a15 and a14 and not a13)  # 110 => RAM2
            ) or (a15 and a14 and a13 and a12 and a11 and not nWR)
        ))
    ]


def do_data_pla(bits):
    (nM1, nIORQ, nRD, a7, a6, a5, a4, a3, a2, a1, a0) = bits

    # nCTC, nDART, nPIO2, nFLOP, nDMA, nPIO1, P16
    is_valid_iorq = nM1 and not nIORQ
    return [
        # select CTC  if address 1110 11xx == ec-ef and valid IORQ
        not (is_valid_iorq and a7 and a6 and a5 and not a4 and a3 and a2),

        # select DART if address 1111 00xx == f0-f3 and valid IORQ
        not (is_valid_iorq and a7 and a6 and a5 and a4 and not a3 and not a2),

        # select PIO2 if address 1111 01xx == f4-f7 and valid IORQ
        not (is_valid_iorq and a7 and a6 and a5 and a4 and not a3 and a2),

        # select FLOP if address 1111 10xx == f8-fb and valid IORQ
        not (is_valid_iorq and a7 and a6 and a5 and a4 and a3 and not a2),

        # select DMA  if address 1111 11xx == fc-ff and valid IORQ
        not (is_valid_iorq and a7 and a6 and a5 and a4 and a3 and a2),

        # select PIO1 if address 1110 10xx == e8-eb and valid IORQ
        not (is_valid_iorq and a7 and a6 and a5 and not a4 and a3 and not a2),

        ((not nIORQ) and (not nM1 or a7)) or nRD
    ]


pla_imps = {
    'address': do_address_pla,
    'data': do_data_pla,
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pla_type', choices=list(pla_imps))
    args = parser.parse_args()

    do_pla = pla_imps[args.pla_type]

    print(f'Reading PLA JSON dump from stdin for {args.pla_type} mode...')
    mappings = json.load(sys.stdin)
    print(f'Records read: {len(mappings)}')

    errors = 0
    for (bits_in, bits_out) in mappings.items():
        bits = bitstring_to_bools(bits_in)
        actual = bools_to_bitstring(do_pla(bits))
        if actual != bits_out:
            print(f'Error @{bits_in}: -{bits_out} +{actual}')
            errors += 1
    print(f'Errors: {errors}')
    return 1 if errors != 0 else 0


if __name__ == "__main__":
    exit(main())
