#!/usr/bin/env python

import json
import random
import sys
import time
import greatfet

# these are for 50005
#address_names = [
#    # lsb first
#    'J7_P2', # A0
#    'J7_P3', # A1
#    'J7_P6', # A2
#    'J7_P7', # A3
#    'J7_P8', # A4
#    'J7_P13', # A5
#    'J7_P14', # A6
#    'J7_P15', # A7
#    'J7_P16', # nRD
#    'J7_P17', # nIORQ
#    'J7_P18', # nM1
#]
#
#cs_names = [
#    # lsb first
#    'J2_P4',  # P16 to U4AP19
#    'J2_P6',  # PIO1
#    'J2_P8',  # DMA
#    'J2_P10', # FLOP
#    'J2_P14', # PIO2
#    'J2_P16', # DART
#    'J2_P18', # CTC
#]

# these are for 50031
address_names = [
    # lsb first
    'J7_P2', # A11
    'J7_P3', # A12
    'J7_P6', # A13
    'J7_P7', # A14
    'J7_P8', # A15
    'J7_P13', # nMREQ
    'J7_P14', # nWR
    'J7_P15', # nRFSH
    'J7_P16', # U1AP16 into our P19
    #'J7_P17',
    #'J7_P18',
]

cs_names = [
    # lsb first
    'J2_P4',  # M_A11
    'J2_P6',  # BP_D_DIR
    'J2_P8',  # combined A14 or nWE
    'J2_P10', # P15, NC on PCB
    'J2_P14', # nCE_EE
    'J2_P16', # nCE_RAM2
    'J2_P18', # nCE_RAM1
    'J2_P20', # nCE_RAM0
    'J2_P22', # nCE_ROM
]


# lines is list of port bits, lsb first
def write_port(lines, value):
    for line in lines:
        bitval = bool(value & 1)
        line.write(bitval)
        value = value >> 1

# lines is list of port bits, lsb first
def read_port(lines):
    value = 0
    for line in reversed(lines):
        value = value << 1
        value |= (1 & line.read())

    return value

gf = greatfet.GreatFET()

address_pins = []
for name in address_names:
    gpio = gf.gpio.get_pin(name)
    gpio.set_direction(gf.gpio.DIRECTION_OUT)
    address_pins.append(gpio)
a_len = len(address_pins)

cs_pins = []
for name in cs_names:
    gpio = gf.gpio.get_pin(name)
    gpio.set_direction(gf.gpio.DIRECTION_IN)
    cs_pins.append(gpio)
cs_len = len(cs_pins)

mappings = dict()

# first pass
for i in range(2**len(address_pins)):
    write_port(address_pins, i)
    time.sleep(0.005)
    port_value = read_port(cs_pins)
    mappings[i] = port_value
    print(f'{i:0{a_len}b}: {port_value:0{cs_len}b}', file=sys.stderr)

errors = 0
for i in range(1):
    # random pass
    print('# Random pass to verify', file=sys.stderr)
    addresses = list(mappings)
    random.shuffle(addresses)
    for i in addresses:
        write_port(address_pins, i)
        read_value = read_port(cs_pins)
        expected_value = mappings[i]
        if expected_value != read_value:
            print(f'# Error: {i:0{a_len}b}: -{expected_value:0{cs_len}b} +{read_value:0{cs_len}b}', file=sys.stderr)
            errors += 1
        else:
            print(f'# {i:0{a_len}b} ok', file=sys.stderr)
    print(f'# Verification errors: {errors}', file=sys.stderr)

bitstring_map = dict()
for (n_in, n_out) in mappings.items():
    bits_in = f'{n_in:0{a_len}b}'
    bits_out = f'{n_out:0{cs_len}b}'
    bitstring_map[bits_in] = bits_out

print(json.dumps(bitstring_map))

exit(errors != 0)
