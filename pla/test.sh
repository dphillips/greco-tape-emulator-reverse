#!/bin/sh -xe

cd "$(dirname "$0")"
pytest --doctest-modules --ignore=pla-scan.py
./pla-sim.py data < u1a.json
./pla-sim.py address < u4a.json
